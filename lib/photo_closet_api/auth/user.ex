defmodule PhotoClosetApi.Auth.User do
  use Ecto.Schema
  import Ecto.Changeset
  # alias PhotoClosetApi.Auth.User
  alias Comeonin.Bcrypt

  schema "users" do
    field :login_id, :string
    field :name, :string
    field :password, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :login_id, :password])
    |> validate_required([:name, :login_id, :password])
    |> unique_constraint([:login_id])
    |> validate_length(:password, min: 6)
    |> put_pass_hash()
  end

  defp put_pass_hash(%Ecto.Changeset{valid?: true, changes: %{password: password}} = chgset) do
    change(chgset, password: Bcrypt.hashpwsalt(password))
  end
  defp put_pass_hash(chgset), do: chgset
end
