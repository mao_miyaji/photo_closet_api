defmodule PhotoClosetApi.AuthPipeline do
    @claims %{typ: "access"}

    use Guardian.Plug.Pipeline, otp_app: :Phx13Gdn10,
                                 module: PhotoClosetApi.Auth.Guardian,
                                 error_handler: PhotoClosetApi.AuthErrorHandler

    plug Guardian.Plug.VerifySession, claims: @claims
    plug Guardian.Plug.VerifyHeader, claims: @claims, realm: "Bearer"
    plug Guardian.Plug.EnsureAuthenticated
    plug Guardian.Plug.LoadResource, ensure: true
end

defmodule PhotoClosetApi.AuthErrorHandler do
    import Plug.Conn

    def auth_error(conn, {type, _}, _opts) do
        body = Poison.encode!(%{message: to_string(type)})
        send_resp(conn, 401, body)
    end
end