defmodule PhotoClosetApi.Repo.Migrations.CreateRelGoodTagGood do
    use Ecto.Migration

    def change do
        create table(:rel_good_tags_good) do
        add :good_id, :integer, null: false
        add :good_tag_id, :integer, null: false

        timestamps()
        end

        create index("rel_good_tags_good", [:good_id])
        create index("rel_good_tags_good", [:good_tag_id])
    end
end
