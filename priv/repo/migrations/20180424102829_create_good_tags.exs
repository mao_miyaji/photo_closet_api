defmodule PhotoClosetApi.Repo.Migrations.CreateGoodTags do
    use Ecto.Migration

    def change do
    create table(:good_tags) do
        add :name, :string, null: false

        timestamps()
    end

    create unique_index(:good_tags, [:name])
    end
end
