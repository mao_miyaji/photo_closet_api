defmodule PhotoClosetApi.Repo.Migrations.CreateGood do
  use Ecto.Migration

  def change do
    create table(:goods) do
      add :user_id, :integer, null: false
      add :category_id, :integer, default: 0
      add :name, :string, null: false
      add :detail, :map
      add :purchase_at, :datetime
      add :favorite_flag, :boolean, null: false, default: 0
      add :image_path, :string

      timestamps()
    end

    create index("goods", [:user_id])
    create index("goods", [:category_id])
  end
end
