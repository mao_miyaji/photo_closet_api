defmodule PhotoClosetApi.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :login_id, :string
      add :password, :string

      timestamps()
    end
    create unique_index(:users, [:login_id])

  end
end
