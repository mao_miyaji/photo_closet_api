defmodule PhotoClosetApi.Router do
    use PhotoClosetApi.Web, :router

    pipeline :browser do
        plug :accepts, ["html"]
        plug :fetch_session
        plug :fetch_flash
        plug :protect_from_forgery
        plug :put_secure_browser_headers
    end

    pipeline :api do
        plug :accepts, ["json"]
        plug CORSPlug
    end

    pipeline :authenticated do
        plug PhotoClosetApi.AuthPipeline
    end

    scope "/", PhotoClosetApi do
        pipe_through :browser # Use the default browser stack
        get "/", PageController, :index
    end

    # Other scopes may use custom stacks.
    scope "/api", PhotoClosetApi do
        pipe_through :api
        post "/auth/login", SessionController, :login
        post "/auth/logout", SessionController, :logout
        post "/auth/refresh_token", SessionController, :refresh_token
        options "/auth/login", SessionController, :options
        options "/auth/logout", SessionController, :options
        options "/auth/refresh_token", SessionController, :options

        # pipe_through :authenticated
        resources "/users", UserController, except: [:new, :edit]
        options "/users", UserController, :options
        options "/users/:id", UserController, :options

        resources "/goods", GoodController, except: [:new, :edit]
        post "/goods/search/tag", GoodController, :search_by_tag
        options "/goods", GoodController, :options
        options "/goods/:id", GoodController, :options
        options "/goods/search/tag", GoodController, :options

        resources "/good-tags", GoodTagController, except: [:new, :edit]
        options "/good-tags", GoodTagController, :options
        options "/good-tags/:id", GoodTagController, :options

        resources "/categories", CategoryController, except: [:new, :edit]
        options "/categories", CategoryController, :options
        options "/categories/:id", CategoryController, :options
    end
end
