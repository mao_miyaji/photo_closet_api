defmodule PhotoClosetApi.RelGoodTagGood do
    use PhotoClosetApi.Web, :model

    schema "rel_good_tags_good" do
        field :good_id, :integer
        field :good_tag_id, :integer

        timestamps()
    end

    @doc """
    Builds a changeset based on the `struct` and `params`.
    """
    def changeset(struct, params \\ %{}) do
        struct
        |> cast(params, [:good_id, :good_tag_id])
        |> validate_required([:good_id, :good_tag_id])
    end
end
