defmodule PhotoClosetApi.User do
  use PhotoClosetApi.Web, :model

  alias Comeonin.Bcrypt

  schema "users" do
    field :name, :string
    field :login_id, :string
    field :password, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(model, params) do
    model
    |> cast(params, [:name, :login_id, :password])
    |> validate_required([:name, :login_id, :password])
    |> unique_constraint(:login_id)
    |> validate_length(:password, min: 6)
    |> put_pass_hash()
  end

  defp put_pass_hash(%Ecto.Changeset{valid?: true, changes: %{password: password}} = chgset) do
    change(chgset, password: Bcrypt.hashpwsalt(password))
  end
  defp put_pass_hash(chgset), do: chgset
end