defmodule PhotoClosetApi.Good do
    use PhotoClosetApi.Web, :model

    alias PhotoClosetApi.Repo

    alias PhotoClosetApi.RelGoodTagGood
    alias PhotoClosetApi.GoodTag

    schema "goods" do
        field :user_id, :integer
        field :category_id, :integer
        field :name, :string
        field :detail, :string
        field :purchase_at, Ecto.DateTime
        field :favorite_flag, :integer
        field :image_path, :string

        timestamps()
    end

    @doc """
    Builds a changeset based on the `struct` and `params`.
    """
    def changeset(struct, params \\ %{}) do
        struct
        |> cast(params, [:user_id, :category_id, :name, :detail, :purchase_at, :favorite_flag, :image_path])
        |> validate_required([:user_id, :name])
    end

    @doc """
    # タグ一覧取得
        @param integer id
    """
    def getTags(id) do
        tags = GoodTag
            |> join(:inner, [gt], rgtg in RelGoodTagGood, gt.id == rgtg.good_tag_id)
            |> where([gt, rgtg], rgtg.good_id == ^id)
            |> Repo.all()
        tags
    end
end
