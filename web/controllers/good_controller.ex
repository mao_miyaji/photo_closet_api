defmodule PhotoClosetApi.GoodController do
    use PhotoClosetApi.Web, :controller

    alias PhotoClosetApi.Good
    # alias PhotoClosetApi.GoodTag
    alias PhotoClosetApi.RelGoodTagGood

    def index(conn, _params) do
        goods = Repo.all(Good)
        # タグ一覧も入れ込む
        goods = Enum.map(goods, fn(x) -> Map.put(x, :tags, Good.getTags(x.id)) end)
        render(conn, "index.json", goods: goods)
    end

    def create(conn, %{"param" => good_params}) do
        changeset = Good.changeset(%Good{}, good_params)

        case Repo.insert(changeset) do
            {:ok, good} ->
                conn
                |> put_status(:created)
                |> render("show.json", good: good)
            {:error, changeset} ->
                conn
                |> put_status(:unprocessable_entity)
                |> render(PhotoClosetApi.ChangesetView, "error.json", changeset: changeset)
        end
    end

    def show(conn, %{"id" => id}) do
        good = Repo.get!(Good, id)
        # タグ一覧も入れ込む
        good = Map.put(good, :tags, Good.getTags(id))

        render(conn, "show.json", good: good)
    end

    def update(conn, %{"id" => id, "param" => good_params}) do
        good = Repo.get!(Good, id)
        changeset = Good.changeset(good, good_params)

        # reset_relation(id)
        # set_tags(id, good_params)

        case Repo.update(changeset) do
            {:ok, good} ->
                render(conn, "show.json", good: good)
            {:error, changeset} ->
                conn
                |> put_status(:unprocessable_entity)
                |> render(PhotoClosetApi.ChangesetView, "error.json", changeset: changeset)
        end
    end

    def delete(conn, %{"id" => id}) do
        good = Repo.get!(Good, id)

        # Here we use delete! (with a bang) because we expect
        # it to always work (and if it does not, it will raise).
        Repo.delete!(good)

        send_resp(conn, :no_content, "")
    end

    # タグから検索
    def search_by_tag(conn, %{"param" => %{"good_tag_ids" => good_tag_ids}}) do
        goods = Good
            |> join(:inner, [g], rgtg in RelGoodTagGood, g.id == rgtg.good_id)
            |> where([g, rgtg], rgtg.good_tag_id in ^good_tag_ids)
            |> Repo.all()

        render(conn, "index.json", goods: goods)
    end

    # # タグ紐付けリセット
    # defp reset_relation(good_id) do
    #     rels = RelGoodTagGood
    #         |> where([rgtg], rgtg.good_id == ^good_id)
    #         |> Repo.all()
            
    #     if Enum.count(rels) > 0 do
    #         Repo.delete_all(rels)
    #     end
    # end

    # # タグ紐付け設定
    # defp set_tags(id, params) do
    #     if Map.get(params, :tag_names) do
    #         Enum.each(params.tag_names,
    #             fn(x) ->
    #                 # タグ登録
    #                 goodTag = setTag(x)
    #                 # 紐付き登録
    #                 changeset = RelGoodTagGood.changeset(%RelGoodTagGood{}, %{"good_id" => id, "good_tag_id" => goodTag.id})
    #                 Repo.insert(changeset)
    #                 # case Repo.insert(changeset) do
    #                 #     {:ok, goodTag} ->
    #                 #     {:error, changeset} ->
    #                 # end
    #         end)
    #     end
    # end

    # # タグ設定
    # defp setTag(name) do
    #     goodTag = Repo.get_by(GoodTag, name: name)
    #     if !goodTag do
    #         changeset = GoodTag.changeset(%GoodTag{}, %{"name" => name })
    #         case Repo.insert(changeset) do
    #             {:ok, goodTag} -> goodTag
    #             # {:error, _} ->
    #         end
    #     end
    #     goodTag
    # end
end
