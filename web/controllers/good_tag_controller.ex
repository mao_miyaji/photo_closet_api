defmodule PhotoClosetApi.GoodTagController do
    use PhotoClosetApi.Web, :controller

    alias PhotoClosetApi.GoodTag

    def index(conn, _params) do
        goodTags = Repo.all(GoodTag)
        render(conn, "index.json", good_tags: goodTags)
    end

    def create(conn, %{"param" => goodTag_params}) do
        changeset = GoodTag.changeset(%GoodTag{}, goodTag_params)

        case Repo.insert(changeset) do
            {:ok, goodTag} ->
                conn
                |> put_status(:created)
                |> render("show.json", good_tag: goodTag)
            {:error, changeset} ->
                conn
                |> put_status(:unprocessable_entity)
                |> render(PhotoClosetApi.ChangesetView, "error.json", changeset: changeset)
        end
    end

    def show(conn, %{"id" => id}) do
        goodTag = Repo.get!(GoodTag, id)
        render(conn, "show.json", good_tag: goodTag)
    end

    def update(conn, %{"id" => id, "param" => goodTag_params}) do
        goodTag = Repo.get!(GoodTag, id)
        changeset = GoodTag.changeset(goodTag, goodTag_params)

        case Repo.update(changeset) do
            {:ok, goodTag} ->
                render(conn, "show.json", good_tag: goodTag)
            {:error, changeset} ->
                conn
                |> put_status(:unprocessable_entity)
                |> render(PhotoClosetApi.ChangesetView, "error.json", changeset: changeset)
        end
    end

    def delete(conn, %{"id" => id}) do
        goodTag = Repo.get!(GoodTag, id)

        # Here we use delete! (with a bang) because we expect
        # it to always work (and if it does not, it will raise).
        Repo.delete!(goodTag)

        send_resp(conn, :no_content, "")
    end
end
