defmodule PhotoClosetApi.PageController do
  use PhotoClosetApi.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
