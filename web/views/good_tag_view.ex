defmodule PhotoClosetApi.GoodTagView do
    use PhotoClosetApi.Web, :view

    def render("index.json", %{good_tags: good_tags}) do
        %{data: render_many(good_tags, PhotoClosetApi.GoodTagView, "good_tag.json")}
    end

    def render("show.json", %{good_tag: good_tag}) do
        %{data: render_one(good_tag, PhotoClosetApi.GoodTagView, "good_tag.json")}
    end

    def render("good_tag.json", %{good_tag: good_tag}) do
        %{
            id: good_tag.id,
            name: good_tag.name
        }
    end
end
