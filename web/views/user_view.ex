defmodule PhotoClosetApi.UserView do
    use PhotoClosetApi.Web, :view

    def render("index.json", %{users: users}) do
        %{data: render_many(users, PhotoClosetApi.UserView, "user.json")}
    end

    def render("show.json", %{user: user}) do
        %{data: render_one(user, PhotoClosetApi.UserView, "user.json")}
    end

    def render("user.json", %{user: user}) do
        %{id: user.id, name: user.name, login_id: user.login_id}
    end
end
