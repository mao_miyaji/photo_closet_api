defmodule PhotoClosetApi.GoodView do
    use PhotoClosetApi.Web, :view

    def render("index.json", %{goods: goods}) do
        %{data: render_many(goods, PhotoClosetApi.GoodView, "good.json")}
    end

    def render("show.json", %{good: good}) do
        %{data: render_one(good, PhotoClosetApi.GoodView, "good.json")}
    end

    def render("good.json", %{good: good}) do
        %{
            id: good.id,
            category_id: good.category_id,
            name: good.name,
            detail: good.detail,
            purchase_at: good.purchase_at,
            favorite_flag: good.favorite_flag,
            image_path: good.image_path,
            tags: getTagsRender(good)
        }
    end

    # タグ一覧のjson取得
    defp getTagsRender(good) do
        if Map.get(good, :tags) do
            tags = render_many(good.tags, PhotoClosetApi.GoodTagView, "good_tag.json")
        else
            tags = []
        end
        tags
    end
end
