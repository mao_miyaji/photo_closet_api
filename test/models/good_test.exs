defmodule PhotoClosetApi.GoodTest do
  use PhotoClosetApi.ModelCase

  alias PhotoClosetApi.Good

  @valid_attrs %{name: "some name", purchase_at: "some purchase_at", user_id: "some user_id"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Good.changeset(%Good{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Good.changeset(%Good{}, @invalid_attrs)
    refute changeset.valid?
  end
end
