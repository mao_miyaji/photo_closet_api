defmodule PhotoClosetApi.GoodTagTest do
  use PhotoClosetApi.ModelCase

  alias PhotoClosetApi.GoodTag

  @valid_attrs %{name: "some name"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = GoodTag.changeset(%GoodTag{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = GoodTag.changeset(%GoodTag{}, @invalid_attrs)
    refute changeset.valid?
  end
end
