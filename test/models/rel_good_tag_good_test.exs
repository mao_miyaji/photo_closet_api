defmodule PhotoClosetApi.RelGoodTagGoodTest do
  use PhotoClosetApi.ModelCase

  alias PhotoClosetApi.RelGoodTagGood

  @valid_attrs %{good_id: 42, good_tag_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = RelGoodTagGood.changeset(%RelGoodTagGood{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = RelGoodTagGood.changeset(%RelGoodTagGood{}, @invalid_attrs)
    refute changeset.valid?
  end
end
