use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :photo_closet_api, PhotoClosetApi.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :photo_closet_api, PhotoClosetApi.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: "root",
  password: "",
  database: "photo_closet_api_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
