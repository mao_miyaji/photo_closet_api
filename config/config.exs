# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :photo_closet_api,
  ecto_repos: [PhotoClosetApi.Repo]

# Configures the endpoint
config :photo_closet_api, PhotoClosetApi.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "/Nw35Iezd8fp1mLfMRSaS2CPna/q/d4+Rj+ghdcHyPmmdAti+xgbgBGTbI/MJwNX",
  render_errors: [view: PhotoClosetApi.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PhotoClosetApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configures Guardian
# config :guardian, Guardian,
#   issuer: "PhotoClosetApi",
#   ttl: {2, :hours},
#   allowed_drift: 2000,
#   secret_key: "LcY8s+rMXKDl1xyEpEiE5WSIdoEpnV+SVqTse6awlzWu8MqULJKUA2AeHWizjZJu",
#   serializer: PhotoClosetApi.Guardian.Serializer

config :guardian, Guardian.DB,
  repo: PhotoClosetApi.Repo,
  schema_name: "guardian_tokens",
  sweep_interval: 60

config :photo_closet_api, PhotoClosetApi.Auth.Guardian,
  issuer: "PhotoClosetApi",
  secret_key: "cISqmSkTDo4EdzpIU73bPhgukNf20mtdI+DTPjsGlJspTvTqlIbp/3XodHmGxYly"

  # Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
